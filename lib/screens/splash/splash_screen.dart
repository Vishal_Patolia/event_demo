import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:splash_screen_view/SplashScreenView.dart';
import '../home/view/home_screen.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
            child: SplashScreenView(
          navigateRoute: const HomeScreen(),
          duration: 3000,
          imageSize: 250,
          imageSrc: "assets/images/flutter_logo.png",
          text: 'app_name'.tr,
          textType: TextType.TyperAnimatedText,
          textStyle: GoogleFonts.gothicA1(
              color: Colors.black,
              fontSize: 40,
              fontWeight: FontWeight.w700,
              letterSpacing: 0.3),
          colors: const [Colors.black, Colors.black, Colors.black],
          backgroundColor: Colors.white,
        )),
      ),
    );
  }
}
