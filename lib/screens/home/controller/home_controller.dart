
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_project/models/response/search_event_response.dart';

import '../../../services/remote_services.dart';
import '../../../utils/app_constants.dart';
import '../../../utils/extensions.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class HomeController extends GetxController {
  var isLoading = true.obs;
  RxList<Events> searchEventList = <Events>[].obs;
  var totalEvent = 0;
  var currentTotalEvent = 0;

  // For Pagination
  ScrollController scrollController = ScrollController();

  var isMoreDataAvailable = true.obs;

  void searchEventListing({String? searchQuery}) async {
   var page = 1;
    try {
      isMoreDataAvailable(false);
      isLoading(true);
      await RemoteServices.searchEvent(searchQuery!, page)
          .then((value) {
            searchEventList.clear();
            searchEventList.value =
                (value.events!.isNotEmpty ? value.events : [])!;
            totalEvent = value.meta!.total!;
            currentTotalEvent = value.events!.length;
            paginateTask(searchQuery, page);
          })
          .onError((error, stackTrace) {
            isLoading(false);
            snackBar('failure'.tr, error.obs.string, Colors.red.shade500);
          })
          .timeout(Duration(seconds: AppConstant.requestTime))
          .whenComplete(() {
            isLoading(false);
          });
    } finally {
      isLoading(false);
    }
  }

  // For Pagination
  void paginateTask(String searchQuery, int page) {
    //Current page size is greater than or equal total page
    if (currentTotalEvent <= totalEvent) {
      scrollController.addListener(() {
        if (scrollController.position.pixels ==
            scrollController.position.maxScrollExtent) {
          page++;
          getMoreTask(searchQuery: searchQuery, page: page);
        }
      });
    }
  }

  // Get More data
  void getMoreTask({String? searchQuery, int? page}) async {
    try {
      isMoreDataAvailable(true);
      isLoading(false);
      await RemoteServices.searchEvent(searchQuery!, page!)
          .then((value) {
            if (value.events!.isNotEmpty) {
              isMoreDataAvailable(true);
              totalEvent = value.meta!.total!;
              currentTotalEvent = value.events!.length;
              searchEventList
                  .addAll(value.events!.isNotEmpty ? value.events! : []);
            } else {
              isMoreDataAvailable(false);
            }
          })
          .onError((error, stackTrace) {
            isMoreDataAvailable(false);
            isLoading(false);
            snackBar('failure'.tr, error.obs.string, Colors.red.shade500);
          })
          .timeout(Duration(seconds: AppConstant.requestTime))
          .whenComplete(() {
            isMoreDataAvailable(false);
            isLoading(false);
          });
    } finally {
      isMoreDataAvailable(false);
      isLoading(false);
    }
  }

  @override
  void onClose() {
    super.onClose();
    scrollController.dispose();
  }
}
