import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:test_project/screens/home/widgets/custom_appbar.dart';
import 'package:test_project/values/dimens.dart';

import '../../../values/style.dart';
import '../controller/home_controller.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class HomeDetailsScreen extends StatelessWidget {
  final HomeController homeController;
  final int? index;

  const HomeDetailsScreen({Key? key, required this.homeController, this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(homeController: homeController, index: index),
      body: SafeArea(
          child: Padding(
              padding: const EdgeInsets.only(
                  top: spacingStandard,
                  bottom: spacingStandard,
                  left: spacingContainer,
                  right: spacingContainer),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  eventImage(
                      homeController.searchEventList[index!].performers!
                                  .isNotEmpty &&
                              homeController.searchEventList[index!]
                                      .performers![0].image !=
                                  null
                          ? homeController
                              .searchEventList[index!].performers![0].image!
                          : '',
                      context),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: spacingStandard,
                          right: spacingStandard,
                          top: spacingContainer,
                          bottom: spacingContainer),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                              homeController.searchEventList[index!].venue !=
                                          null &&
                                      homeController.searchEventList[index!]
                                              .datetimeLocal !=
                                          null
                                  ? DateFormat("EEE, dd MMM yyyy HH:mm a")
                                      .format(
                                          DateFormat("yyyy-MM-dd'T'HH:mm:ss")
                                              .parse(homeController
                                                  .searchEventList[index!]
                                                  .datetimeLocal!))
                                  : '',
                              style: boldTextStyle,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis),
                          const SizedBox(height: spacingControlHalf),
                          Text(
                              homeController.searchEventList[index!].venue !=
                                          null &&
                                      homeController.searchEventList[index!]
                                              .venue!.displayLocation !=
                                          null
                                  ? homeController.searchEventList[index!]
                                      .venue!.displayLocation!
                                  : '',
                              style: smallNormalTextStyle,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis),
                        ],
                      ),
                    ),
                  )
                ],
              ))),
    );
  }

  appbarWidget(String appTitle, var context) {
    return Obx(() => AppBar(
        titleSpacing: 5,
        elevation: 0.0,
        title: Text(appTitle,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            softWrap: true,
            style: appBarStyle),
        actions: [
          GestureDetector(
            onTap: () {
              if (homeController.searchEventList[index!].favoriteStatus!) {
                homeController.searchEventList[index!].favoriteStatus = false;
              } else {
                homeController.searchEventList[index!].favoriteStatus = true;
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                  homeController.searchEventList[index!].favoriteStatus!
                      ? Icons.favorite
                      : Icons.favorite_border_rounded,
                  color: homeController.searchEventList[index!].favoriteStatus!
                      ? Colors.red
                      : Colors.grey),
            ),
          )
        ],
        leading: InkWell(
          borderRadius: BorderRadius.circular(20.0),
          child: const Icon(Icons.arrow_back, size: 24, color: Colors.white),
          onTap: () => moveToLast(context),
        )));
  }

  moveToLast(var context) {
    Get.back();
  }

  eventImage(String images, BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(8.0)),
      child: Image.network(
        images,
        fit: BoxFit.cover,
        errorBuilder: (context, url, error) =>
            Image.asset('assets/images/flutter_logo.png', fit: BoxFit.contain),
        width: MediaQuery.of(context).size.width,
        scale: 2,
        height: 250,
      ),
    ).paddingOnly(top: spacingStandard);
  }
}
