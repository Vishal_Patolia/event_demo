import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:test_project/models/response/search_event_response.dart';
import 'package:test_project/screens/home/view/home_details.dart';
import 'package:test_project/values/dimens.dart';
import 'package:test_project/widgets/error_view.dart';

import '../../../utils/extensions.dart';
import '../../../values/style.dart';
import '../../../widgets/loading_overlay.dart';
import '../controller/home_controller.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final HomeController homeController = Get.put(HomeController());
  TextEditingController searchTextController = TextEditingController();
  Timer? _debounce;
  String? lastInputValue;

  @override
  void initState() {
    super.initState();

    ///Call API to Get Data
    apiCall();
  }

  Widget searchField() {
    return Padding(
      padding: const EdgeInsets.all(spacingContainer),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.grey,
        cursorHeight: 20,
        controller: searchTextController,
        cursorWidth: 2,
        cursorRadius: const Radius.circular(8.0),
        textInputAction: TextInputAction.go,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(FocusNode());
          /*Get.to(() => ProductSearchScreen(searchText: value));*/
        },
        onChanged: (value) {
          _onSearchChanged(value);
        },
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(18.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.5)),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.5)),
            ),
            hintText: 'search_hint'.tr,
            hintStyle: GoogleFonts.gothicA1(
              color: Colors.black.withOpacity(0.8),
              fontSize: 16.0,
              fontWeight: FontWeight.w400,
            ),
            suffixIcon: IconButton(
              onPressed: () => {},
              icon: InkWell(
                  child: const Icon(Icons.cancel_rounded,
                      color: Colors.grey, size: 24.0),
                  onTap: () {
                    if (searchTextController.text.isNotEmpty) {
                      FocusScope.of(context).unfocus();
                      searchTextController.clear();
                      homeController.searchEventList.clear();
                      apiCall();
                    }
                  }),
            ),
            fillColor: Colors.white,
            filled: true),
      ),
    );
  }

  _onSearchChanged(String value) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      check().then((connection) {
        if (connection) {
          if (lastInputValue != value) {
            lastInputValue = value;
            homeController.searchEventListing(searchQuery: value);
          }
          return;
        } else {
          snackBar('no_connection'.tr, 'no_connection_avail'.tr,
              Colors.red.shade500);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar: appbarWidget('Event', context, true),
      body: SafeArea(child: Obx(() {
        return Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                searchField(),
                homeController.searchEventList.isNotEmpty
                    ? Expanded(
                        child: ListView.separated(
                            controller: homeController.scrollController,
                            scrollDirection: Axis.vertical,
                            itemCount: homeController.searchEventList.length,
                            shrinkWrap: true,
                            physics: const ClampingScrollPhysics(),
                            separatorBuilder: (context, index) {
                              return Container(height: 0.5, color: Colors.grey);
                            },
                            itemBuilder: (context, index) {
                              if (index ==
                                      homeController.searchEventList.length -
                                          1 &&
                                  homeController.isMoreDataAvailable.value ==
                                      true) {
                                return const Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: Center(
                                      child: CircularProgressIndicator()),
                                );
                              }
                              return InkWell(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: spacingStandard,
                                        bottom: spacingStandard,
                                        left: spacingStandard,
                                        right: spacingContainer),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        eventImage(
                                            homeController
                                                        .searchEventList[index]
                                                        .performers!
                                                        .isNotEmpty &&
                                                    homeController
                                                            .searchEventList[
                                                                index]
                                                            .performers![0]
                                                            .image !=
                                                        null
                                                ? homeController
                                                    .searchEventList[index]
                                                    .performers![0]
                                                    .image!
                                                : '',
                                            index),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(
                                                spacingStandard),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                    homeController
                                                            .searchEventList[
                                                                index]
                                                            .title ??
                                                        '',
                                                    style: boldTextStyle,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis),
                                                Text(
                                                    homeController
                                                                    .searchEventList[
                                                                        index]
                                                                    .venue !=
                                                                null &&
                                                            homeController
                                                                    .searchEventList[
                                                                        index]
                                                                    .venue!
                                                                    .displayLocation !=
                                                                null
                                                        ? homeController
                                                            .searchEventList[
                                                                index]
                                                            .venue!
                                                            .displayLocation!
                                                        : '',
                                                    style: smallNormalTextStyle,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis),
                                                const SizedBox(
                                                    height: spacingControlHalf),
                                                Text(
                                                    homeController
                                                                    .searchEventList[
                                                                        index]
                                                                    .venue !=
                                                                null &&
                                                            homeController
                                                                    .searchEventList[
                                                                        index]
                                                                    .datetimeLocal !=
                                                                null
                                                        ? DateFormat(
                                                                "EEE, dd MMM yyyy HH:mm a")
                                                            .format(DateFormat(
                                                                    "yyyy-MM-dd'T'HH:mm:ss")
                                                                .parse(
                                                                    homeController
                                                                        .searchEventList[
                                                                            index]
                                                                        .datetimeLocal!))
                                                        : '',
                                                    style: smallNormalTextStyle
                                                        .copyWith(
                                                            fontSize:
                                                                textSizeSmall),
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis)
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    )),
                                onTap: () {
                                  Get.to(() => HomeDetailsScreen(
                                      homeController: homeController,
                                      index: index));
                                },
                              );
                            }),
                      )
                    : Expanded(
                        child: ErrorView(
                          title: 'no_data'.tr,
                          subTitle: 'no_data_found_msg'.tr,
                        ),
                      ),
              ],
            ),
            homeController.isLoading.value &&
                    homeController.isMoreDataAvailable.value == false
                ? const LoadingOverlay(isLoading: true)
                : Container(),
          ],
        );
      })),
    );
  }

  eventImage(String images, int eventIndex) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
            child: Image.network(
              images,
              fit: BoxFit.cover,
              errorBuilder: (context, url, error) => Image.asset(
                  'assets/images/flutter_logo.png',
                  height: 70,
                  width: 80),
              width: 80,
              height: 70,
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          child: GestureDetector(
            onTap: () {
              if (homeController.searchEventList[eventIndex].favoriteStatus!) {
                homeController.searchEventList[eventIndex].favoriteStatus =
                    false;
              } else {
                homeController.searchEventList[eventIndex].favoriteStatus =
                    true;
              }
              homeController.searchEventList.refresh();
            },
            child: Icon(
                homeController.searchEventList[eventIndex].favoriteStatus!
                    ? Icons.favorite
                    : Icons.favorite_border_rounded,
                color:
                    homeController.searchEventList[eventIndex].favoriteStatus!
                        ? Colors.red
                        : Colors.grey),
          ),
        ),
      ],
    );
  }

  apiCall() {
    check().then((connection) {
      if (connection) {
        homeController.isLoading(true);
        homeController.searchEventListing(
            searchQuery: searchTextController.text);
      } else {
        snackBar(
            'no_connection'.tr, 'no_connection_avail'.tr, Colors.red.shade500);
      }
    });
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }
}
