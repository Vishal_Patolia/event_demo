import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../values/values.dart';
import '../controller/home_controller.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final HomeController? homeController;
  final int? index;

  const CustomAppBar({Key? key, this.homeController, this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => AppBar(
        titleSpacing: 5,
        elevation: 0.0,
        title: Text(homeController!.searchEventList[index!].title!,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            softWrap: true,
            style: appBarStyle),
        actions: [
          GestureDetector(
            onTap: () {
              if (homeController!.searchEventList[index!].favoriteStatus!) {
                homeController!.searchEventList[index!].favoriteStatus = false;
              } else {
                homeController!.searchEventList[index!].favoriteStatus = true;
              }
              homeController!.searchEventList.refresh();
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                  homeController!.searchEventList[index!].favoriteStatus!
                      ? Icons.favorite
                      : Icons.favorite_border_rounded,
                  color: homeController!.searchEventList[index!].favoriteStatus!
                      ? Colors.red
                      : Colors.grey),
            ),
          )
        ],
        leading: InkWell(
          borderRadius: BorderRadius.circular(20.0),
          child: const Icon(Icons.arrow_back, size: 24, color: Colors.white),
          onTap: () => Get.back(),
        )));
  }

  @override
  Size get preferredSize => const Size.fromHeight(55.0);
}
