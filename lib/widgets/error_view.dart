import 'package:flutter/material.dart';

import '../utils/app_constants.dart';
import '../values/dimens.dart';
import '../values/style.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class ErrorView extends StatelessWidget {
  final String? image, title, subTitle, retry;
  final bool? isRetry;
  final VoidCallback? onPress;

  const ErrorView(
      {Key? key,
      this.image,
      this.title,
      this.subTitle,
      this.isRetry = false,
      this.retry,
      this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
    image != null
        ? Image.asset(image!,
            fit: BoxFit.scaleDown,
            width: AppConstant.imageSize,
            height: AppConstant.imageSize)
        : Container(),
    Padding(
      padding: const EdgeInsets.only(
          top: spacingContainer * 2,
          left: spacingContainer,
          right: spacingContainer,
          bottom: spacingStandard),
      child: Text(
        title!,
        textAlign: TextAlign.center,
        style: boldLargeTextStyle,
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(
          left: spacingContainer, right: spacingContainer),
      child: Text(subTitle!,
          textAlign: TextAlign.center, style: normalTextStyle),
    ),
    const SizedBox(height: spacingContainer),
    isRetry!
        ? ElevatedButton(
            onPressed: onPress,
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0))),
                backgroundColor: MaterialStateProperty.all(Colors.red)),
            child: Text(retry!,
                style: mediumTextStyle.copyWith(color: Colors.white)),
          )
        : Container(),
      ],
    );
  }
}
