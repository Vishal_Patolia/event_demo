import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../values/colors.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class LoadingOverlay extends StatefulWidget {
  final bool? isLoading;

  const LoadingOverlay({
    Key? key,
    this.isLoading,
  }) : super(key: key);

  @override
  LoadingOverlayState createState() => LoadingOverlayState();
}

class LoadingOverlayState extends State<LoadingOverlay> {
  @override
  Widget build(BuildContext context) {
    if (widget.isLoading!) {
      return Stack(
        children: [
          Container(
            color: Colors.black.withAlpha(100),
          ),
          const Center(child: SpinKitCircle(color: primaryColor, size: 40))
        ],
      );
    }
    return Container();
  }
}
