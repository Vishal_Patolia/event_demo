import 'package:get/get.dart';

import '../screens/home/view/home_screen.dart';
import '../screens/splash/splash_screen.dart';
import 'app_routes.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class AppPages {
  static const splash = Routes.splash;

  static final routes = [
    GetPage(
        name: Routes.splash,
        page: () => const SplashScreen(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.home,
        page: () => const HomeScreen(),
        transition: Transition.fadeIn),
  ];
}
