/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

abstract class Routes {
  static const splash = '/splash';
  static const home = '/home';
}
