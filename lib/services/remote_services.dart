import 'package:test_project/utils/app_constants.dart';

import '../models/response/search_event_response.dart';
import 'base/end_points.dart';
import 'base/http_client.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class RemoteServices {
  static var client = HttpClient(baseUrl);

  /// Search Query With Page Size and Page Number
  static Future<SearchEventResponse> searchEvent(String searchQuery, int page) async {
    var retAuth = await client.getWithPath('${apiSearch}client_id=${AppConstant.clientId}&q=$searchQuery&per_page=${AppConstant.pageSize}&page=$page');
    SearchEventResponse response = SearchEventResponse.fromJson(retAuth);
    return response;
  }
}
