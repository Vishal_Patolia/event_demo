/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class AppException implements Exception {
  final _message;

  AppException([this._message]);

  String toString() {
    return _message;
  }
}

class FetchDataException extends AppException {
  FetchDataException(String message) : super(message);
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message);
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message);
}

class InvalidInputException extends AppException {
  InvalidInputException([String? message]) : super(message);
}

class RequestCanceledException extends AppException {
  RequestCanceledException([String? message]) : super(message);
}

class ServerSideException extends AppException {
  ServerSideException([String? message]) : super(message);
}

class ConnectionException extends AppException {
  ConnectionException([String? message]) : super(message);
}
