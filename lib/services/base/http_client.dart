import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../utils/dio_util_error.dart';
import 'app_exceptions.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class HttpClient {
  late Dio _client;
  String baseUrl = "";

  Map<String, dynamic> headers = {};

  HttpClient(String url) {
    baseUrl = url;
    _client = Dio();
    _client.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
  }

  Future<void> _interceptor() async {
    headers['Content-Type'] = 'application/json';
    _client.options.headers = headers;
    _client.options.followRedirects = true;
    _client.options.validateStatus = (code) {
      return 200 <= code! && code <= 350;
    };
  }

  Future<dynamic> getWithPath(String url) async {
    return get(baseUrl + url);
  }

  Future<dynamic> get(String url) async {
    await _interceptor();
    if (kDebugMode) {
      print('Api Get, url $url');
    }
    dynamic responseJson;
    try {
      final response = await _client.get(url);
      responseJson = returnResponse(response);
    } on DioError catch (e) {
      DioErrorUtil.handleError(e);
    }
    return responseJson;
  }

  dynamic returnResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        return response.data;
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:
      case 403:
      case 302:
        throw UnauthorisedException(response.data.toString());
      case 500:
      default:
        throw FetchDataException('${response.statusCode}');
    }
  }
}
