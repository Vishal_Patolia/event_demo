import 'dart:math';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../values/style.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

appbarWidget(String appTitle, var context, bool isHome) {
  return AppBar(
      titleSpacing: 5,
      elevation: 0.0,
      title: Text(appTitle,maxLines: 2,
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          style: appBarStyle),
      leading: isHome == false ? InkWell(
        borderRadius: BorderRadius.circular(20.0),
        child: const Icon(Icons.arrow_back, size: 24, color: Colors.white),
        onTap: () => moveToLast(context),
      ): const Icon(Icons.home, size: 24, color: Colors.white));
}

moveToLast(var context) {
  Get.back();
}

snackBar(String title, String msg, Color color) {
  return Get.snackbar(title, msg,
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: color,
      colorText: Colors.white,
      messageText:
          Text(msg, style: smallMediumTextStyle.copyWith(color: Colors.white)),
      titleText:
          Text(title, style: mediumTextStyle.copyWith(color: Colors.white)),
      margin: const EdgeInsets.symmetric(horizontal: 8),
      snackStyle: SnackStyle.GROUNDED);
}

Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}

extension NumX on num {
  double roundDecimal(int places) {
    double? mod = pow(10.0, places) as double?;
    return ((this * mod!).round().toDouble() / mod);
  }

  String roundDecimalString(int places) {
    double? mod = pow(10.0, places) as double?;
    return ((this * mod!).round().toDouble() / mod).toStringAsFixed(places);
  }
}

// String
extension StringX on String {
  ///Returns first letter of the string as Caps eg -> Flutter
  String firstLetterUpperCase() => length > 1
      ? "${this[0].toUpperCase()}${substring(1).toLowerCase()}"
      : this;

  /// Return a bool if the string is null or empty
  bool get isEmptyOrNull => isEmpty;

  /// Returns the string if it is not `null`, or the empty string otherwise
  String get orEmpty => this;

  // if the string is empty perform an action
  String ifEmpty(Function action) => isEmpty ? action() : this;

  bool validateEmail() => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(this);
}
