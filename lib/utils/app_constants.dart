/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class AppConstant {
  static const int pageSize = 20;
  static double imageSize = 250;
  static int requestTime = 220;
  static String clientId = 'Mjg0NjcyMjF8MTY2MDU0NTAxOC40NzU2MDMz';
}
