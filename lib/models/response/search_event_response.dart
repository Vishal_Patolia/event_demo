/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class SearchEventResponse {
  List<Events>? events;
  Meta? meta;

  SearchEventResponse({this.events, this.meta});

  SearchEventResponse.fromJson(Map<String, dynamic> json) {
    if (json['events'] != null) {
      events = <Events>[];
      json['events'].forEach((v) {
        events!.add(Events.fromJson(v));
      });
    }
    meta = json['meta'] != null ? Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (events != null) {
      data['events'] = events!.map((v) => v.toJson()).toList();
    }
    if (meta != null) {
      data['meta'] = meta!.toJson();
    }
    return data;
  }
}

class Events {
  String? type;
  int? id;
  String? datetimeUtc;
  Venue? venue;
  bool? datetimeTbd;
  List<Performers>? performers;
  bool? isOpen;
  String? datetimeLocal;
  bool? timeTbd;
  String? shortTitle;
  String? visibleUntilUtc;
  String? url;
  String? announceDate;
  String? createdAt;
  bool? dateTbd;
  String? title;
  String? description;
  bool? favoriteStatus = false;

  Events(
      this.type,
      this.id,
      this.datetimeUtc,
      this.venue,
      this.datetimeTbd,
      this.performers,
      this.isOpen,
      this.datetimeLocal,
      this.timeTbd,
      this.shortTitle,
      this.visibleUntilUtc,
      this.url,
      this.announceDate,
      this.createdAt,
      this.dateTbd,
      this.title,
      this.description);

  Events.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    id = json['id'];
    datetimeUtc = json['datetime_utc'];
    venue = (json['venue'] != null ? Venue.fromJson(json['venue']) : null)!;
    datetimeTbd = json['datetime_tbd'];
    if (json['performers'] != null) {
      performers = <Performers>[];
      json['performers'].forEach((v) {
        performers!.add(Performers.fromJson(v));
      });
    }
    isOpen = json['is_open'];
    datetimeLocal = json['datetime_local'];
    timeTbd = json['time_tbd'];
    shortTitle = json['short_title'];
    visibleUntilUtc = json['visible_until_utc'];
    url = json['url'];
    announceDate = json['announce_date'];
    createdAt = json['created_at'];
    dateTbd = json['date_tbd'];
    title = json['title'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = type;
    data['id'] = id;
    data['datetime_utc'] = datetimeUtc;
    data['venue'] = venue!.toJson();
    data['datetime_tbd'] = datetimeTbd;
    data['performers'] = performers!.map((v) => v.toJson()).toList();
    data['is_open'] = isOpen;
    data['datetime_local'] = datetimeLocal;
    data['time_tbd'] = timeTbd;
    data['short_title'] = shortTitle;
    data['visible_until_utc'] = visibleUntilUtc;
    data['url'] = url;
    data['announce_date'] = announceDate;
    data['created_at'] = createdAt;
    data['date_tbd'] = dateTbd;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class Venue {
  String? state;
  String? nameV2;
  String? postalCode;
  String? name;
  String? timezone;
  String? url;
  String? address;
  String? country;
  bool? hasUpcomingEvents;
  String? city;
  String? slug;
  String? extendedAddress;
  String? displayLocation;

  Venue(
      this.state,
      this.nameV2,
      this.postalCode,
      this.name,
      this.timezone,
      this.url,
      this.address,
      this.country,
      this.hasUpcomingEvents,
      this.city,
      this.slug,
      this.extendedAddress,
      this.displayLocation);

  Venue.fromJson(Map<String, dynamic> json) {
    state = json['state'];
    nameV2 = json['name_v2'];
    postalCode = json['postal_code'];
    name = json['name'];
    timezone = json['timezone'];
    url = json['url'];
    address = json['address'];
    country = json['country'];
    hasUpcomingEvents = json['has_upcoming_events'];
    city = json['city'];
    slug = json['slug'];
    extendedAddress = json['extended_address'];
    displayLocation = json['display_location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['state'] = state;
    data['name_v2'] = nameV2;
    data['postal_code'] = postalCode;
    data['name'] = name;
    data['timezone'] = timezone;
    data['url'] = url;
    data['address'] = address;
    data['country'] = country;
    data['has_upcoming_events'] = hasUpcomingEvents;
    data['city'] = city;
    data['slug'] = slug;
    data['extended_address'] = extendedAddress;
    data['display_location'] = displayLocation;
    return data;
  }
}

class Performers {
  String? type;
  String? name;
  String? image;
  String? shortName;
  String? imageLicense;
  String? imageRightsMessage;
  bool? homeTeam;
  bool? awayTeam;
  List<Genres>? genres;

  Performers(
      this.type,
      this.name,
      this.image,
      this.shortName,
      this.imageLicense,
      this.imageRightsMessage,
      this.homeTeam,
      this.awayTeam,
      this.genres);

  Performers.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    name = json['name'];
    image = json['image'];
    shortName = json['short_name'];
    imageLicense = json['image_license'];
    imageRightsMessage = json['image_rights_message'];
    homeTeam = json['home_team'];
    awayTeam = json['away_team'];
    if (json['genres'] != null) {
      genres = <Genres>[];
      json['genres'].forEach((v) {
        genres!.add(Genres.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = type;
    data['name'] = name;
    data['image'] = image;
    data['short_name'] = shortName;
    data['image_license'] = imageLicense;
    data['image_rights_message'] = imageRightsMessage;
    data['home_team'] = homeTeam;
    data['away_team'] = awayTeam;
    data['genres'] = genres!.map((v) => v.toJson()).toList();
    return data;
  }
}

class Genres {
  String? name;
  String? slug;
  bool? primary;
  String? image;

  Genres(this.name, this.slug, this.primary, this.image);

  Genres.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    slug = json['slug'];
    primary = json['primary'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['slug'] = slug;
    data['primary'] = primary;
    data['image'] = image;
    return data;
  }
}

class Meta {
  int? total;
  int? took;
  int? page;
  int? perPage;

  Meta(
    this.total,
    this.took,
    this.page,
    this.perPage,
  );

  Meta.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    took = json['took'];
    page = json['page'];
    perPage = json['per_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['total'] = total;
    data['took'] = took;
    data['page'] = page;
    data['per_page'] = perPage;
    return data;
  }
}
