import 'package:flutter/material.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class AppColor {
  static const itemBackgroundColor = Color(0xFFD1D1D1);
}

const accentColor = Color(0xFFbb74ed);
const accentSecondary = Color(0xFF353436);
const primaryColor = Color(0xff7429e5);
const primaryColorDark = Color(0xff6724cc);
const primaryTextColor = Color(0xFF212121);
const secondaryTextColor = Color(0xFF515151);
const textBackgroundColor = Color(0xFFEDEDED);
const itemBackgroundColor = Color(0xFFD1D1D1);
const editTextBackgroundColor = Color(0xFFf5f5f5);
const editTextHintColor = Color(0xFFcbcbcb);
const btnBgTextColor = Color(0xFF707070);
const Color backgroundColor = Color(0xFFFFFFFF);
const Color error = Color(0xFFD9534F);
const Color success = Color(0xFF5CB85C);
const Color warning = Color(0xFFE89E35);
const Color info = Color(0xFF59BFDD);
