import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/values/values.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

final appBarStyle = GoogleFonts.gothicA1(
  color: Colors.white,
  fontSize: textSizeMedium,
  fontWeight: FontWeight.bold,
);

final hintTextStyle = GoogleFonts.gothicA1(
  color: Colors.grey.withOpacity(0.7),
  fontSize: textSizeMedium,
  fontWeight: FontWeight.bold,
);

//Font Size Small and Color Black
final smallNormalTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeSMedium,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3);
final smallMediumTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeSMedium,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.3);
final smallBoldTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeSMedium,
    fontWeight: FontWeight.w700,
    letterSpacing: 0.3);

//Font Size Normal and Color Black
final normalTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeMedium,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3);
final mediumTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeMedium,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.3);
final boldTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeMedium,
    fontWeight: FontWeight.w700,
    letterSpacing: 0.3);

//Font Size Large and Color Black
final normalLargeTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeLargeMedium,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3);
final mediumLargeTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeLargeMedium,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.3);
final boldLargeTextStyle = GoogleFonts.gothicA1(
    color: Colors.black,
    fontSize: textSizeLargeMedium,
    fontWeight: FontWeight.w700,
    letterSpacing: 0.3);

final semiSubTitleTextStyle = GoogleFonts.gothicA1(
    fontSize: textSizeSMedium,
    letterSpacing: 0.3,
    color: secondaryTextColor,
    fontWeight: FontWeight.w400); // Semi sub title with regular

