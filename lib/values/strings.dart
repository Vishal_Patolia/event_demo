import 'package:get/get_navigation/src/root/internacionalization.dart';

/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

class Strings extends Translations {
  @override
  Map<String, Map<String, String>> get keys =>
      {
        'en_US': {
          'app_name': 'Event App',
          'failure': 'Failure',
          'no_connection': 'No Connection',
          'no_connection_avail': 'No Connection Available',
          'search_hint': 'Search',
          'no_data': 'No data found',
          'no_data_found_msg': 'No event data available'
        },
      };
}