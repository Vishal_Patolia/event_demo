/// Created by Vishal Patel
/// Copyright (c) 2022-23. All rights reserved.

//MARGIN
const double spacingControl = 4.0;
const double spacingStandard = 8.0;
const double spacingContainer = 16.0;
const double buttonHeight = 60.0;
const double cardCircularRadius = 12.0;
const double spacingControlHalf = 4.0;

//FONTS
const textSizeSmall = 12.0;
const textSizeSMedium = 14.0;
const textSizeMedium = 16.0;
const textSizeLargeMedium = 18.0;
const textSizeNormal = 20.0;
const textSizeLarge = 24.0;
const textSizeXLarge = 30.0;

const double textRadius = 50.0;
const double editTextRadius = 6.0;
const double keyboardRadius = 40.0;
const double keyboardIconHW = 80.0;

const double elevation = 4.0;

const double homeSliderHeight = 200;
const double homeProductHeight = 195;
